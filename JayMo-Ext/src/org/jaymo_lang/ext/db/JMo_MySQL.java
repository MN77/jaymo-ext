/*******************************************************************************
 * Copyright (C) 2018-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.db;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.ext.db.login.Login_MySQL;
import de.mn77.ext.db.sql.A_SqlDB;
import de.mn77.ext.db.sql.MySQL;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_MySQL extends A_SqlDatabase {

	private final ArgCallBuffer host, db, user, pass;


	/**
	 * ! MySQL is a a Client for a MySQL-Server
	 * + MySQL(Str host, Str db, Str user, Str pass)
	 */
	public JMo_MySQL( final Call host, final Call db, final Call user, final Call pass ) {
		this.host = new ArgCallBuffer( 0, host );
		this.db = new ArgCallBuffer( 1, db );
		this.user = new ArgCallBuffer( 2, user );
		this.pass = new ArgCallBuffer( 3, pass );
	}

	@Override
	public void init( final CallRuntime cr ) {
		this.host.init( cr, this, JMo_Str.class );
		this.db.init( cr, this, JMo_Str.class );
		this.user.init( cr, this, JMo_Str.class );
		this.pass.init( cr, this, JMo_Str.class );
	}

	@Override
	protected A_SqlDB openDatabase( final CallRuntime cr ) {
		final String sHost = ((JMo_Str)this.host.get()).rawString();
		final String sUser = ((JMo_Str)this.user.get()).rawString();
		final String sPass = ((JMo_Str)this.pass.get()).rawString();
		final String sDB = ((JMo_Str)this.db.get()).rawString();

		final MySQL db = new MySQL();
		final Login_MySQL login = new Login_MySQL( sHost, sDB, sUser, sPass );

		try {
			db.connect( login );
		}
		catch( final Exception e ) {
			throw new ExternalError( cr, "Database connect error!", e.getMessage() );
		}

		return db;
	}

}
