/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.page.item;

import java.awt.Color;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.ext.pdf.create.IText_Calculator;
import org.jaymo_lang.ext.pdf.create.constant.JMo_IText_Font;
import org.jaymo_lang.ext.pdf.create.page.I_IText_PageData;
import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Error;

import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;

import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;


/**
 * @author Michael Nitsche
 * @implNote TODO: Different underline/strike styles
 */
public class JMo_IText_ItemText extends A_ObjectSimple implements I_IText_PageItem {

	private static final float UNDERLINE_OFFSET_FACTOR = 2.0f;
	private static final float LINE_THICKNESS_DIV = 1.5f;
	private static final float STRIKE_OFFSET_DIV = 3.8f;

	private POSITION_H     alignment;
	private boolean        bold, italic, underline, strikeThrough;
	private JMo_IText_Font font;
	private int            fontsize;
	private int[]          rgb;
	private final String   text;
	private final float    x, y;


	public JMo_IText_ItemText( final float x, final float y, final String text, final String defaultFont, final Integer defaultFontSize, final int[] defaultColorFont,
		final POSITION_H defaultFontAlign ) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.font = new JMo_IText_Font();
		this.fontsize = defaultFontSize != null ? defaultFontSize : 12;
		this.rgb = defaultColorFont != null ? defaultColorFont : new int[]{ 0, 0, 0 };
		this.alignment = defaultFontAlign != null ? defaultFontAlign : POSITION_H.LEFT;
		this.bold = false;
		this.italic = false;
	}

	public void draw( final CallRuntime cr, final I_IText_PageData page, final PdfContentByte cb, final IText_Calculator calc ) {
		final Color color = new Color( this.rgb[0], this.rgb[1], this.rgb[2] );
		cb.setColorFill( color );
		cb.beginText();

		final BaseFont baseFont = this.font.getBaseFont( cr, this.bold, this.italic );
		cb.setFontAndSize( baseFont, this.fontsize );

		int align = PdfContentByte.ALIGN_LEFT;
		if( this.alignment == POSITION.RIGHT )
			align = PdfContentByte.ALIGN_RIGHT;
		if( this.alignment == POSITION.CENTER )
			align = PdfContentByte.ALIGN_CENTER;
		cb.showTextAligned( align, this.text, calc.x( this.x ), calc.y( page, this.y ), 0 );
		cb.endText();

		// Lines
		if( this.underline || this.strikeThrough ) {
	        float textWidth = baseFont.getWidthPoint(this.text, this.fontsize);
			cb.setColorStroke( color );

			final float fontSizeNorm = this.fontsize / 12f;
			final float lineWidth = fontSizeNorm / JMo_IText_ItemText.LINE_THICKNESS_DIV;
			cb.setLineWidth( lineWidth );

			// Draw underline
			if(this.underline) {
				final float lineOffset = fontSizeNorm * JMo_IText_ItemText.UNDERLINE_OFFSET_FACTOR;
				cb.moveTo(calc.x( this.x ), calc.y( page, this.y ) - lineOffset);
				cb.lineTo(calc.x( this.x ) + textWidth, calc.y( page, this.y ) - lineOffset);
				cb.stroke();
			}

			// Draw strike
			if(this.strikeThrough) {
				final float lineOffset = this.fontsize / JMo_IText_ItemText.STRIKE_OFFSET_DIV;
				cb.moveTo(calc.x( this.x ), calc.y( page, this.y ) + lineOffset);
				cb.lineTo(calc.x( this.x ) + textWidth, calc.y( page, this.y ) + lineOffset);
				cb.stroke();
			}
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setColor":
				return this.mSetColor( cr );
			case "setAlignment":
			case "setAlign":
				return this.mSetAlignment( cr );
			case "alignLeft":
				return this.mAlignLeft( cr );
			case "alignCenter":
				return this.mAlignCenter( cr );
			case "alignRight":
				return this.mAlignRight( cr );
			case "bold":
				return this.mBold( cr );
			case "italic":
				return this.mItalic( cr );
			case "underline":
				return this.mUnderline( cr );
			case "strike":
			case "strikeThrough":
				return this.mStrikeThrough( cr );
			case "setSize":
//			case "setFontSize":
			case "setFontsize":
				return this.mSetFontsize( cr );
			case "setFont":
				return this.mSetFont( cr );
		}
		return null;
	}

	/**
	 * °alignCenter()Same # Align this text to the center.
	 */
	private JMo_IText_ItemText mAlignCenter( final CallRuntime cr ) {
		cr.argsNone();
		this.alignment = POSITION_H.CENTER;
		return this;
	}

	/**
	 * °alignLeft()Same # Align this text to the left.
	 */
	private JMo_IText_ItemText mAlignLeft( final CallRuntime cr ) {
		cr.argsNone();
		this.alignment = POSITION_H.LEFT;
		return this;
	}

	/**
	 * °alignRight()Same # Align this text to the right.
	 */
	private JMo_IText_ItemText mAlignRight( final CallRuntime cr ) {
		cr.argsNone();
		this.alignment = POSITION_H.RIGHT;
		return this;
	}

	/**
	 * °bold()Same # Make this text bold.
	 */
	private JMo_IText_ItemText mBold( final CallRuntime cr ) {
		cr.argsNone();
		this.bold = true;
		return this;
	}

	/**
	 * °italic()Same # Make this text italic.
	 */
	private JMo_IText_ItemText mItalic( final CallRuntime cr ) {
		cr.argsNone();
		this.italic = true;
		return this;
	}

	/**
	 * °underline()Same # Make this text underlined.
	 */
	private JMo_IText_ItemText mUnderline( final CallRuntime cr ) {
		cr.argsNone();
		this.underline = true;
		return this;
	}

	/**
	 * °strike ^ strikeThrough
	 * °strikeThrough()Same # Make this text strike through.
	 */
	private JMo_IText_ItemText mStrikeThrough( final CallRuntime cr ) {
		cr.argsNone();
		this.strikeThrough = true;
		return this;
	}

	/**
	 * °setAlign ^ setAlignment
	 * °setAlignment(MagicPosition align)Same # Set the alignment (__LEFT, __CENTER, __RIGHT) for this text.
	 */
	private JMo_IText_ItemText mSetAlignment( final CallRuntime cr ) {
		final MagicPosition arg = (MagicPosition)cr.args( this, MagicPosition.class )[0];
		final POSITION pos = arg.get();
		if( !Lib_Position.isHorizontal( pos ) )
			throw new RuntimeError( cr, "Invalid position", "Allowed are only horizontal positions" );
		this.alignment = Lib_Position.getHorizontal( pos );
		return this;
	}

	/**
	 * °setColor(Color color)Same # Set the text color
	 * °setColor(Int r, Int g, Int b)Same # Set the text color
	 */
	private JMo_IText_ItemText mSetColor( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.rgb = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setFont(IText_Font font)Same # Set the font for this text.
	 */
	private JMo_IText_ItemText mSetFont( final CallRuntime cr ) {
		this.font = (JMo_IText_Font)cr.args( this, JMo_IText_Font.class )[0];
		return this;
	}

	/**
	 * °setSize ^ setFontSize
	 * °setFontsize(Int size)Same # Set the fontsize for this text.
	 */
	private JMo_IText_ItemText mSetFontsize( final CallRuntime cr ) {
		final JMo_Int arg = (JMo_Int)cr.args( this, JMo_Int.class )[0];
		Lib_Error.ifNotBetween( cr, 4, 150, this.fontsize, "Fontsize" );
		this.fontsize = arg.rawInt( cr );
		return this;
	}

}
