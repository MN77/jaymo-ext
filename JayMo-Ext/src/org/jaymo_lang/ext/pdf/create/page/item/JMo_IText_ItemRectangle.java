/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.page.item;

import java.awt.Color;

import org.jaymo_lang.ext.pdf.create.IText_Calculator;
import org.jaymo_lang.ext.pdf.create.page.I_IText_PageData;
import org.jaymo_lang.lib.graphic.Util_Color;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_Number;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import com.lowagie.text.pdf.PdfContentByte;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_ItemRectangle extends A_ObjectSimple implements I_IText_PageItem {

	private int[]       colorFill, colorBorder;
	private boolean     drawFilling, drawBorder;
	private Float       thickness;
	private final float x, y, dx, dy;


	public JMo_IText_ItemRectangle( final float x, final float y, final float dx, final float dy, final Float defaultLineThickness, final int[] defaultColorLine, final int[] defaultColorFill ) {
		this.x = x;
		this.dx = dx;
		this.y = y;
		this.dy = dy;
		this.thickness = defaultLineThickness;
		this.colorBorder = defaultColorLine != null ? defaultColorLine : new int[]{ 0, 0, 0 };
		this.colorFill = defaultColorFill != null ? defaultColorFill : new int[]{ 0, 0, 0 };
		this.drawFilling = false;
		this.drawBorder = true;
	}

	public void draw( final CallRuntime cr, final I_IText_PageData page, final PdfContentByte cb, final IText_Calculator calc ) {
		if( this.thickness == null )
			this.thickness = calc.reverse( 1 );

		if( this.drawFilling ) {
			cb.setColorFill( new Color( this.colorFill[0], this.colorFill[1], this.colorFill[2] ) );
//			cb.rectangle(this.x, page.getHeight() - this.y - this.dy, this.dx, this.dy);
			cb.rectangle( calc.x( this.x ), calc.y( page, this.y ) - calc.o( this.dy ), calc.o( this.dx ), calc.o( this.dy ) );
			cb.fill();
			cb.stroke();
		}

		if( this.drawBorder ) {
			cb.setColorStroke( new Color( this.colorBorder[0], this.colorBorder[1], this.colorBorder[2] ) );
			cb.setLineWidth( calc.o( this.thickness ) );
//			cb.rectangle(this.x, page.getHeight() - this.y - this.dy, this.dx, this.dy);
			cb.rectangle( calc.x( this.x ), calc.y( page, this.y ) - calc.o( this.dy ), calc.o( this.dx ), calc.o( this.dy ) );
			cb.stroke();
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "setStyle":
				return this.mSetStyle( cr );
			case "setThickness":
				return this.mSetThickness( cr );
			case "setFillColor":
			case "setColorFill":
				return this.mSetFillColor( cr );
			case "setBorderColor":
			case "setColorBorder":
			case "setLineColor":
				return this.mSetBorderColor( cr );
		}
		return null;
	}

	/**
	 * °setLineColor ^ setColorBorder
	 * °setBorderColor ^ setColorBorder
	 * °setColorBorder(Color color)Same # Set the border color of this rectangle
	 * °setColorBorder(Int r, Int g, Int b)Same # Set the border color of this rectangle
	 */
	private JMo_IText_ItemRectangle mSetBorderColor( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.colorBorder = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setFillColor ^ setColorFill
	 * °setColorFill(Color color)Same # Set the fill color for this rectangle
	 * °setColorFill(Int r, Int g, Int b)Same # Set the fill color for this rectangle
	 */
	private JMo_IText_ItemRectangle mSetFillColor( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 3 );
		this.colorFill = Util_Color.argsToRGB( cr, args );
		return this;
	}

	/**
	 * °setStyle(Bool drawFilling, Bool drawBorder)Same # Set the style of this rectangle.
	 */
	private JMo_IText_ItemRectangle mSetStyle( final CallRuntime cr ) {
		final I_Object[] args = cr.args( this, JMo_Bool.class, JMo_Bool.class );
		final boolean drawFilling = Lib_Convert.toBoolean( cr, args[0] );
		final boolean drawBorder = Lib_Convert.toBoolean( cr, args[1] );

		if( !drawFilling && !drawBorder )
			cr.warning( "Invalid arguments", "A rectangle without filling and border will not be drawed!" );
		this.drawFilling = drawFilling;
		this.drawBorder = drawBorder;
		return this;
	}

	/**
	 * °setThickness(Number thickness)Same # Set the thickness of the border line for this rectangle
	 */
	private JMo_IText_ItemRectangle mSetThickness( final CallRuntime cr ) {
		final I_Number arg = (I_Number)cr.args( this, I_Number.class )[0];
		final float f = Lib_Convert.toFloat( cr, arg );
		Lib_Error.ifNotBetween( cr, 0, 50, f, "thickness" );
		this.thickness = f;
		return this;
	}

}
