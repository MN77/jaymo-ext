/*******************************************************************************
 * Copyright (C) 2021-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.pdf.create.constant;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;

import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;


/**
 * @author Michael Nitsche
 */
public class JMo_IText_PageSize extends A_Immutable {

	public static final Rectangle DEFAULT = PageSize.A4; // LETTER ?!?

	private final Rectangle size;


	/**
	 * +IText_PageSize()
	 */
	public JMo_IText_PageSize() {
		this.size = JMo_IText_PageSize.DEFAULT;
	}

	public JMo_IText_PageSize( final Rectangle rect ) {
		this.size = rect;
	}

	@Override
	public boolean equals( final Object other ) {
		if( other instanceof JMo_IText_PageSize )
			return this.size.equals( other );
		return false;
	}

	@Override
	public boolean equalsLazy( final Object other ) {
		return this.equals( other );
	}

	/**
	 * °A0()Enum # Constant for page size A0
	 * °A1()Enum # Constant for page size A1
	 * °A2()Enum # Constant for page size A2
	 * °A3()Enum # Constant for page size A3
	 * °A4()Enum # Constant for page size A4
	 * °A5()Enum # Constant for page size A5
	 * °A6()Enum # Constant for page size A6
	 * °A7()Enum # Constant for page size A7
	 * °A8()Enum # Constant for page size A8
	 * °A9()Enum # Constant for page size A9
	 * °A10()Enum # Constant for page size A10
	 *
	 * °B0()Enum # Constant for page size B0
	 * °B1()Enum # Constant for page size B1
	 * °B2()Enum # Constant for page size B2
	 * °B3()Enum # Constant for page size B3
	 * °B4()Enum # Constant for page size B4
	 * °B5()Enum # Constant for page size B5
	 * °B6()Enum # Constant for page size B6
	 * °B7()Enum # Constant for page size B7
	 * °B8()Enum # Constant for page size B8
	 * °B9()Enum # Constant for page size B9
	 * °B10()Enum # Constant for page size B10
	 *
	 * °LETTER()Enum # Constant for page size LETTER
	 * °HALFLETTER()Enum # Constant for page size HALFLETTER
	 * °LEGAL()Enum # Constant for page size LEGAL
	 * °POSTCARD()Enum # Constant for page size POSTCARD
	 */
	@Override
	public A_Immutable getConstant( final CallRuntime cr, final String name ) {

		switch( name ) {
			case "A0":
				return new JMo_IText_PageSize( PageSize.A0 );
			case "A1":
				return new JMo_IText_PageSize( PageSize.A1 );
			case "A2":
				return new JMo_IText_PageSize( PageSize.A2 );
			case "A3":
				return new JMo_IText_PageSize( PageSize.A3 );
			case "A4":
				return new JMo_IText_PageSize( PageSize.A4 );
			case "A5":
				return new JMo_IText_PageSize( PageSize.A5 );
			case "A6":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "A7":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "A8":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "A9":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "A10":
				return new JMo_IText_PageSize( PageSize.A6 );

			case "B0":
				return new JMo_IText_PageSize( PageSize.A0 );
			case "B1":
				return new JMo_IText_PageSize( PageSize.A1 );
			case "B2":
				return new JMo_IText_PageSize( PageSize.A2 );
			case "B3":
				return new JMo_IText_PageSize( PageSize.A3 );
			case "B4":
				return new JMo_IText_PageSize( PageSize.A4 );
			case "B5":
				return new JMo_IText_PageSize( PageSize.A5 );
			case "B6":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "B7":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "B8":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "B9":
				return new JMo_IText_PageSize( PageSize.A6 );
			case "B10":
				return new JMo_IText_PageSize( PageSize.A6 );

			case "LETTER":
				return new JMo_IText_PageSize( PageSize.LETTER );
			case "HALFLETTER":
				return new JMo_IText_PageSize( PageSize.HALFLETTER );
			case "LEGAL":
				return new JMo_IText_PageSize( PageSize.LEGAL );
			case "POSTCARD":
				return new JMo_IText_PageSize( PageSize.POSTCARD );

			// TODO
		}

		throw new CodeError( cr, "Unknown constant", "Constant/Enum is not defined in <" + this.getTypeName() + ">: " + name );
	}

	public Rectangle getRectangle() {
		return this.size;
	}

	@Override
	public void init( final CallRuntime cr ) {}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {
		return null;
	}

}
