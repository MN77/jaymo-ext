/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.ftp;

import java.util.Calendar;

import org.apache.commons.net.ftp.FTPFile;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Bool;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.datetime.JMo_DateTime;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.datetime.MDateTime;


/**
 * @author Michael Nitsche
 * @created 29.05.2022
 */
public class JMo_FtpRemotePath extends A_ObjectSimple {

	private final FTPFile file;


	protected JMo_FtpRemotePath( final FTPFile file ) {
		this.file = file;
	}

	public FTPFile internalFile() {
		return this.file;
	}

	@Override
	public String toString() {
		return this.file.getName();
	}

	/**
	 * °isFile()Bool # Returns true if this is a file.
	 * °isDir()Bool # Returns true if this is a directory.
	 * °name()Str # Returns the name of this file or directory.
	 * °size()Long # Returns the size of this file.
	 * °user()Str # Returns the user of this file.
	 * °group()Str # Returns the group of this file.
	 * °time ^ timestamp
	 * °timestamp()DateTime # Returns the timestamp of this file.
	 */
	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "isFile":
				cr.argsNone();
				return JMo_Bool.getObject( this.file.isFile() );
//			case "isDirectory":
			case "isDir":
				cr.argsNone();
				return JMo_Bool.getObject( this.file.isDirectory() );
			case "name":
				cr.argsNone();
				return new JMo_Str( this.file.getName() );
			case "size":
				cr.argsNone();
				return new JMo_Long( this.file.getSize() );
			case "user":
				cr.argsNone();
				return new JMo_Str( this.file.getUser() );
			case "group":
				cr.argsNone();
				return new JMo_Str( this.file.getGroup() );
			case "time":
			case "timestamp":
				final Calendar cal = this.file.getTimestamp();
				final MDateTime dt = new MDateTime( cal );
				return new JMo_DateTime( dt );
		}
		return null;
	}

}
