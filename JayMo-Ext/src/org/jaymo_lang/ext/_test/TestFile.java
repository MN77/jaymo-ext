/*******************************************************************************
 * Copyright (C) 2017-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestFile {

	public static void main( final String[] args ) {

		try {
			final Stopwatch uhr = new Stopwatch();
			TestFile.start();
			uhr.print();
		}
		catch( final Throwable t ) {
			Err.exit( t );
		}
	}

	private static void datei( final String s, final String... args ) throws Err_FileSys {
		final MFile d = new MFile( s );
		TestFile.start( d, args );
	}

	private static void start() throws Err_FileSys {
		MOut.setDebug( DEBUG_MODE.NO );

		final String work = "/home/mike/Prog/JayMo/work/";
		final String libWork = "/home/mike/Prog/JayMo/lib/work/";
		final String extAuto = "/home/mike/Prog/JayMo/ext/tests_auto/";
		final String extLocal = "/home/mike/Prog/JayMo/ext/tests_local/";
		final String extMan = "/home/mike/Prog/JayMo/ext/tests_man/";
		final String extWork = "/home/mike/Prog/JayMo/ext/work/";
		final String bricklink = "/home/mike/Prog/JayMo/bricklink/";


//		datei(pfad4+"swing_say4.jmo");
//		datei(pfad4+"neuroph.jmo");

//		TestFile.datei(work + "019/pdfc_cm_zoll.txt");
//		TestFile.datei(ext2 + "strange_3.jmo");
//		TestFile.datei(extMan + "");
//		TestFile.datei(libWork + "calculator_2.jmo");
		TestFile.datei( extMan + "swt-widgets_basic_1.jmo" );
//		TestFile.datei(extWork + "swt_progressbar.jmo");
//		TestFile.datei(extWork + "swt_tree.jmo");
//		TestFile.datei(extWork + "gui-button.jmo");
//		TestFile.datei(extWork + "swt-test.jmo");


//		TestFile.datei(ext2 + "json.jmo");
//		TestFile.datei("/home/mike/Prog/JMo/ext/1_auto/hsqldb_1.jmo");
//		TestFile.datei(bricklink + "tools/add_it.jmo");

//		TestFile.datei("/home/mike/Prog/JMo/bricklink/temp/update_4a_guide1.jmo");
//		TestFile.datei("/home/mike/Prog/JMo/bricklink/" + "add_it.jmo");
	}

	private static void start( final I_File datei, final String... args ) throws Err_FileSys {
		final Parser_App parser = new Parser_App();

		MOut.print( JayMo.NAME + "  " + parser.getVersionString( false, true, true ) );

		final Stopwatch uhr = new Stopwatch();
		MOut.print( "===== " + datei.getName() );
		final App app = parser.parseFile( datei.getFile() );
		MOut.print( uhr );
		MOut.print( "-----------------------------------" );
		app.describe();
		MOut.print( uhr );
		MOut.print( "-----------------------------------" );
//		main.exec();
		app.exec( args );
		MOut.print( uhr );
	}

}
