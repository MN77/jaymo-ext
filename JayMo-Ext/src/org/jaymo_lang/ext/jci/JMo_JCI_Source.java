/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jci;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.jci.readers.FileResourceReader;
import org.apache.commons.jci.readers.MemoryResourceReader;
import org.apache.commons.jci.readers.ResourceReader;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.object.A_ObjectSimple;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 11.06.2022
 */
public class JMo_JCI_Source extends A_ObjectSimple {

	private final ArrayList<String> classes = new ArrayList<>();
	private final ArrayList<String> sources = new ArrayList<>();
	private File                    dir     = null;


	public String[] getClasses() {
		return this.classes.toArray( new String[this.classes.size()] );
	}

	public ResourceReader getReader( final CallRuntime cr ) {
		if( this.classes.size() == 0 )
			throw new ExternalError( cr, "Invalid source definition", "At least one class must be added." );
		if( this.sources.size() > 0 && (this.classes.size() != this.sources.size() || this.dir != null) )
			throw new ExternalError( cr, "Invalid source definition", "It' not possible to mix classes from source with classes from directory." );

		if( this.dir != null )
			return new FileResourceReader( this.dir );
		else {
			final MemoryResourceReader reader = new MemoryResourceReader();
			for( int i = 0; i < this.classes.size(); i++ )
				reader.add( this.classes.get( i ), this.sources.get( i ).getBytes() );
			return reader;
		}
	}

	@Override
	protected I_Object call2( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "add":
				return this.mAdd( cr );
			case "setDir":
				return this.mSetDir( cr );

			default:
				return null;
		}
	}

	/**
	 * °add(Str classpath)Same # Add a class for compiling.
	 * °add(Str classpath, Str source)Same # Add a class with source for compiling.
	 */
	private I_Object mAdd( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 1, 2 );
		final String classpath = Lib_Convert.toStr( cr, args[0] ).rawString();
		this.classes.add( classpath );

		if( args.length == 2 ) {
			final String source = Lib_Convert.toStr( cr, args[1] ).rawString();
			this.sources.add( source );
		}

		return this;
	}

	/**
	 * °setDir(Str|Dir directory)Same # Set source directory
	 */
	private I_Object mSetDir( final CallRuntime cr ) {
		final I_Object arg = cr.argExt( this, JMo_Str.class, JMo_Dir.class );

		if( arg instanceof JMo_Str ) {
			final String s = Lib_Convert.toStr( cr, arg ).rawString();
			this.dir = new File( s );
			if( !this.dir.isDirectory() )
				throw new ExternalError( cr, "Invalid source", "This is not an directory: " + s );
			if( !this.dir.exists() )
				throw new ExternalError( cr, "Invalid source", "Directory not exists: " + s );
		}
		else {
			final JMo_Dir dir = (JMo_Dir)arg;
			this.dir = dir.getInternalFile();
		}
		return this;
	}

}
