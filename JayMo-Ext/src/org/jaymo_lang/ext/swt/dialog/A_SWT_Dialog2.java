/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.dialog;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.jaymo_lang.ext.swt.core.I_SWT_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 *
 *         TODO siehe mn.swt.dialog und mn.swt.komp.einfach.dialog
 */
public abstract class A_SWT_Dialog2<TB extends Dialog> implements I_SWT_Object<TB> {

	private TB swt;


	public TB createSWT( final CallRuntime cr, final Widget vater ) {
		Err.ifNull( vater );
		Err.ifInstanceNot( vater, Shell.class );
		if( !this.isCreated() )
			this.iCreateSWT( (Shell)vater );
		return this.swt;
	}

	public TB swt() {
		return this.swt;
	}

	protected boolean isCreated() {
		return this.swt != null;
	}

	protected abstract TB swtCreate( Shell vater, int style );

	protected abstract void swtInit();

	protected abstract int swtStyle();

	private final TB iCreateSWT( final Shell vater ) {
		this.swt = this.swtCreate( vater, this.swtStyle() );
		Err.ifNull( this.swt );
		this.swtInit();
		return this.swt;
	}

}
