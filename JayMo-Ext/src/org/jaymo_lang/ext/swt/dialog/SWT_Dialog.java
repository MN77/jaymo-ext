/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.dialog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.jaymo_lang.ext.swt.core.I_SWT_Frame;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * TODO Unbedingt umbauen in richtigen Dialog der ein einzelnes Ergebnis zurück gibt!!!
 * TODO Es gibt noch: SWT.ABORT | SWT.RETRY | SWT.IGNORE
 * TODO Standard-Knopf festlegen. Geht aber derzeit mit SWT nicht. Evtl. eigenen Dialog basteln!
 */
public class SWT_Dialog extends A_SWT_Dialog2<MessageBox> {

	private int          icon        = 0, button = 0;
	private Listener     buttonAbort = null;
	private Listener     buttonYes   = null;
	private Listener     buttonNo    = null;
	private Listener     buttonOk    = null;
	private final String text;
	private String       titel       = "";


	public SWT_Dialog( final String text ) {
		this.text = text;
	}

	public void open( final CallRuntime cr ) {
		this.open( cr, true );
	}

	public void open( final CallRuntime cr, final boolean wait ) {
//		if(!(knopf_ja!=null || knopf_nein!=null || knopf_abbr!=null || knopf_ok!=null))
		final Display display = Display.getDefault();
		Shell shell = display.getActiveShell();

		if( shell == null ) {
			final Shell[] shells = Display.getDefault().getShells();
			if( shells.length > 0 )
				shell = shells[0];
		}

		if( shell == null ) {
			MOut.error( this.text );
			MOut.error( "Keine Shell für Dialogfenster vorhanden!" );
//			Err.invalid("Keine Shell vorhanden!");
			return;
		}

		final I_SWT_Frame fenster = (I_SWT_Frame)shell.getData();
		Err.ifNull( fenster );

		final Shell shellw = shell;
		if( wait ) //Thread.sleep, Display.sleep und Schranke gehen hier nicht!
//			iFensterOeffnen(shellw);	//TODO Sehr kritisch! Führt gerne zu Hänger!
			fenster.startSync( cr, () -> {

				try {
					SWT_Dialog.this.iFensterOeffnen( cr, shellw );
				}
				catch( final Throwable t ) {
					Err.show( t );
				}
			} );
		else
			fenster.startAsync( cr, () -> {

				try {
					SWT_Dialog.this.iFensterOeffnen( cr, shellw );
				}
				catch( final Throwable t ) {
					Err.show( t );
				}
			} );
	}

	public SWT_Dialog setListenerAbort( final Listener s ) {
		this.buttonAbort = this.iSetButton( SWT.CANCEL, s );
		return this;
	}

	public SWT_Dialog setListenerNo( final Listener s ) {
		this.buttonNo = this.iSetButton( SWT.NO, s );
		return this;
	}

	public SWT_Dialog setListenerOk( final Listener s ) {
		this.buttonOk = this.iSetButton( SWT.OK, s );
		return this;
	}

	public SWT_Dialog setListenerYes( final Listener s ) {
		this.buttonYes = this.iSetButton( SWT.YES, s );
		return this;
	}

	public SWT_Dialog setTypeError() {
		this.iIcon( SWT.ICON_ERROR );
		return this;
	}

	public SWT_Dialog setTypeInformation() {
		this.iIcon( SWT.ICON_INFORMATION );
		return this;
	}

	public SWT_Dialog setTypeQuestion() {
		this.iIcon( SWT.ICON_QUESTION );
		return this;
	}

	public SWT_Dialog setTypeWarning() {
		this.iIcon( SWT.ICON_WARNING );
		return this;
	}

	@Override
	protected MessageBox swtCreate( final Shell vater, final int style ) {
		return new MessageBox( vater, style );
	}

	@Override
	protected void swtInit() {
		this.swt().setMessage( this.text );
		this.swt().setText( this.titel );
	}

	@Override
	protected int swtStyle() {
		if( this.icon == 0 )
			this.iIcon( SWT.ICON_INFORMATION );
//		if(this.knopf==0) this.i_Knopf(SWT.OK,null);
		return this.icon | this.button;
	}

	// INTERN

	private void iFensterOeffnen( final CallRuntime cr, final Shell shellw ) {
		this.createSWT( cr, shellw );
//		MOut.dev(Display.getDefault().getActiveShell()==null, Display.getDefault().getShells().length);
		final int erg = this.swt().open();
		if( erg == SWT.YES && this.buttonYes != null )
			this.buttonYes.handleEvent( null ); //this
		if( erg == SWT.NO && this.buttonNo != null )
			this.buttonNo.handleEvent( null ); //this
		if( erg == SWT.CANCEL && this.buttonAbort != null )
			this.buttonAbort.handleEvent( null ); //this
		if( erg == SWT.OK && this.buttonOk != null )
			this.buttonOk.handleEvent( null ); //this
	}

	private void iIcon( final int icon ) {
		if( this.icon != 0 )
			Err.invalid( "Typ wurde bereits gesetzt" );
		this.icon = icon;
		if( icon == SWT.ICON_ERROR )
			this.titel = "Fehler";
		if( icon == SWT.ICON_INFORMATION )
			this.titel = "Hinweis";
		if( icon == SWT.ICON_QUESTION )
			this.titel = "Frage";
		if( icon == SWT.ICON_WARNING )
			this.titel = "Warning";
	}

	private Listener iSetButton( final int typ, final Listener neu ) {
		if( (this.button & typ) > 0 )
			Err.invalid( "Knopf wurde bereits gesetzt" );
		this.button |= typ;
		return neu;
	}

}
