/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.utils;

import java.util.HashMap;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.RGB;


/**
 * @author Michael Nitsche
 * @created 19.04.2022
 */
public class SWT_ColorManager {

	private static HashMap<String, Color> stock = new HashMap<>();


	public static Color getColor( final Device d, final int red, final int green, final int blue ) {
		return SWT_ColorManager.iColor( d, red, green, blue );
	}

	public static Color getColor( final Device d, final RGB rgb ) {
		return SWT_ColorManager.iColor( d, rgb.red, rgb.green, rgb.blue );
	}

	public static Color getColor( final int... rgb ) {
		return SWT_ColorManager.getColor( null, rgb[0], rgb[1], rgb[2] );
	}

	public static Color getColor( final RGB rgb ) {
		return SWT_ColorManager.getColor( null, rgb );
	}

	private static Color iColor( final Device d, final int red, final int green, final int blue ) {
		final String key = SWT_ColorManager.iKey( d, red, green, blue );

		if( SWT_ColorManager.stock.keySet().contains( key ) ) {
			Color color = SWT_ColorManager.stock.get( key );

			if( color.isDisposed() ) {
				SWT_ColorManager.stock.remove( key );
				color = SWT_ColorManager.iNew( key, d, red, green, blue );
			}
			return color;
		}
		else
			return SWT_ColorManager.iNew( key, d, red, green, blue );
	}

	private static String iKey( final Device d, final int red, final int green, final int blue ) {
		final String dev = d == null ? null : d.toString();
		final StringBuilder sb = new StringBuilder();
		sb.append( dev );
		sb.append( ',' );
		sb.append( red );
		sb.append( ',' );
		sb.append( green );
		sb.append( ',' );
		sb.append( blue );
		return sb.toString();
	}

	private static Color iNew( final String key, final Device d, final int red, final int green, final int blue ) {
		final Color color = new Color( d, red, green, blue );
		SWT_ColorManager.stock.put( key, color );
		return color;
	}

}
