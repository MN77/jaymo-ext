/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll.composite.canvas.deco;

import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.jaymo_lang.ext.swt.core.I_SWT_Object;
import org.jaymo_lang.ext.swt.core.JMo_SWT_Main;
import org.jaymo_lang.ext.swt.dialog.SWT_Error;
import org.jaymo_lang.ext.swt.graphic.SWT_Image;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.event.Procedure;


public class SWT_Window extends A_SWT_Shell {

//	private enum EVENTS {
//		PRECLOSE
//	}

	protected final I_SWT_Object<?> content;
	private Display                 display;
	private final boolean           flexible;
//	private boolean schliessbar = true;
	private boolean zentriert;


	public SWT_Window( final I_SWT_Object<?> component ) {
		Err.ifNull( component );
		this.content = component;
		this.pendingStyle( SWT.SHELL_TRIM );
		this.flexible = true;
	}

	public SWT_Window( final JMo_SWT_Main component, final boolean fixedSize, final int width, final int height, final String title, final Object... images ) {
		Err.ifNull( component );
		this.content = component;
		this.pendingStyle( SWT.SHELL_TRIM );

		this.flexible = !fixedSize;
		this.setSize( width, height );
		this.setTitle( title );
		if( images != null && images.length > 0 )
			this.setImages( images );
	}

	// BEFEHL

	public void close() {
		this.swt().close();
		this.swt().dispose();
	}

	@Override
//	public SWT_MenueLeiste nMenue_Leiste() {
//		SWT_MenueLeiste leiste=new SWT_MenueLeiste();
//		leiste.createSWT(this.swt());
//		this.swt().setMenuBar(leiste.swt());
//		return leiste;
//	}

	// ERZEUGEN

	// parent Shell wird für Dialoge benötigt
	public void createSWT( final CallRuntime cr, final Display display, final Shell parent ) {
		this.display = display;
		this.createSWT( cr, parent );
	}

	public SWT_Window eVorSchliessen( final Consumer<?> z ) {
//		this.eventAdd(EVENTS.PRECLOSE,z);
		return this;
	}

	@Override
	public void jmoInit3( final CallRuntime cr ) {}

//	public void startTimer(A_SWT_Timer timer) {
//		timer.start(Display.getDefault());
//	}

	public void open( final CallRuntime cr ) {
		this.open( cr, null, null );
	}

	public void open( final CallRuntime cr, final Shell parent ) {
		this.open( cr, parent, null );
	}

	public void open( final CallRuntime cr, final Shell parent, final Procedure bereit ) {
		this.createSWT( cr, Display.getDefault(), parent );
		this.swt().layout();
		if( bereit != null )
			bereit.execute();
	}

	// SETZE

//	public SWT_Window s1_NichtSchliessbar() {
//		this.checkNotCreated();
//		this.schliessbar = false;
//		return this;
//	}

//	public void sAktiv(final boolean b) {
//		this.iAktiv(this.swt().getChildren(), b);
//	}

//	public void sBild(final Object datei) {
//		Err.ifNull(datei);
//		this.pending(() -> {
//			this.swt().setImage(SWT_Image.newDirect(datei).getImage());
//		});
//	}

	/**
	 * Es können mehrere größen übergeben werden! Die Anwendung sucht sich dann das aus was am besten paßt!
	 */
	public SWT_Window setImages( final Object... bilder ) {
		Err.ifNull( bilder );
		this.pending( () -> {
			this.iImages( bilder );
		} );
		return this;
	}

	public void setSize( final int dx, final int dy ) {
		Err.ifOutOfBounds( 10, 2000, dx );
		Err.ifOutOfBounds( 10, 2000, dy );

		this.pending( () -> {
			final Point p = this.swt().computeSize( dx, dy, true );
			this.swt().setSize( p.x, p.y );
			if( this.zentriert )
				this.iCenter();
		} );
	}

	public void setTitle( final String titel ) {
		Err.ifNull( titel );
		this.pending( () -> {
			this.swt().setText( titel );
		} );
	}

	public void startAsync( final CallRuntime cr, final Runnable r ) {
		Err.ifNull( r );

		try {
			this.display.asyncExec( r );
		}
		catch( final Throwable t ) {
			SWT_Error.exit( cr, t );
		}
	}

//	public S_SWT_Fenster sStandartKnopf(S_A_SWT_Steuer<?,Button> knopf) {
//		Err.ifNull(knopf,1);
//		this.swt().setDefaultButton(knopf.swt());
//		return this;
//	}

	public Thread startOutside( final CallRuntime cr, final Runnable r ) {
		Err.ifNull( r );
		Thread erg = null;

		try {
			erg = new Thread( r );
			// BUGFIX: setDaemon: Anwendung blieb sonst hängen wenn noch ein Thread lief
			erg.setDaemon( true );
			erg.start();
		}
		catch( final Throwable t ) {
			SWT_Error.exit( cr, t );
		}
		return erg;
	}

	public void startSync( final CallRuntime cr, final Runnable r ) {
		Err.ifNull( r );

		try {
			this.display.syncExec( r );
		}
		catch( final Throwable t ) {
			SWT_Error.exit( cr, t );
		}
	}

//	@Override
//	protected int swtStyle3() {
//		SHELL_TRIM  = CLOSE | TITLE | MIN | MAX | RESIZE;
//		DIALOG_TRIM = TITLE | CLOSE | BORDER;

//		if(!this.schliessbar) style=style^SWT.CLOSE;
//		if(!this.flexibel) style=(style^SWT.RESIZE)^SWT.MAX;
//		style= style | SWT.BORDER; //Dialog_trim wäre ohne Border, so siehts aber besser aus
//		return style;
//	}

//	public void sZentriert() {
//		this.zentrieren();
//	}

	/**
	 * Zentriert das Fenster auf dem Bildschirm.
	 * Dialoge werden automatisch zentriert, wenn man die Shell übergibt!
	 */
	protected void iCenter() {
		this.zentriert = true;
		final Rectangle monitor = this.display.getPrimaryMonitor().getBounds(); //TODO Fehler!!! Evtl. noch kein Display vorhanden!
//		Rectangle display = this.display.getBounds();
		final Rectangle fenster = this.swt().getBounds();

		final int x = monitor.x + monitor.width / 2 - fenster.width / 2;
		final int y = monitor.y + monitor.height / 2 - fenster.height / 2;
		this.swt().setLocation( x, y );
	}

	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return null;
	}

	/**
	 * TODO Optimieren wegen Performance
	 */
	@Override
	protected Shell swtCreate( final Composite parent, int style ) {
		if( parent != null )
			//			this.s1_Style(SWT.APPLICATION_MODAL|SWT.PRIMARY_MODAL|SWT.SYSTEM_MODAL);
//			this.s1_Style(SWT.DIALOG_TRIM|SWT.APPLICATION_MODAL);
//			this.s1_Style(SWT.APPLICATION_MODAL);
			style = style | SWT.APPLICATION_MODAL;
		if( this.display == null )
			this.display = parent == null ? new Display() : parent.getDisplay();
		if( parent == null )
			return new Shell( this.display, style );
		else
			return new Shell( (Shell)parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {
//		this.swt().addShellListener(new ShellListener() {
//
//			public void shellActivated(final ShellEvent e) {}
//
//			public void shellClosed(final ShellEvent e) { SWT_Fenster.this.eventStart(EVENTS.PRECLOSE,null); }
//
//			public void shellDeactivated(final ShellEvent e) {}
//
//			public void shellDeiconified(final ShellEvent e) {}
//
//			public void shellIconified(final ShellEvent e) {}
//		});

//		Err.ifNull(manager());

//		if(position!=null)
//			if(position==POSITION.CENTER) this.zentrieren();
//			else Err.invalid("POSITION derzeit nicht unterstützt: "+position);

		this.swt().setLayout( new FillLayout() );
		this.content.createSWT( cr, this.swt() );
		this.swt().open();
	}


//	private void iAktiv(final Control[] ca, final boolean b) {
//		for(final Control c : ca) {
//			if(c instanceof Composite)
//				this.iAktiv(((Composite)c).getChildren(), b);
//			c.setEnabled(b);
//		}
//	}

	private void iImages( final Object[] bilder ) {
		Err.ifNull( bilder );
		final I_List<Image> il = new SimpleList<>( bilder.length );
		for( final Object i : bilder )
			il.add( SWT_Image.newDirect( i ).getImage() );
		this.swt().setImages( il.toArray( Image.class ) );
	}

}
