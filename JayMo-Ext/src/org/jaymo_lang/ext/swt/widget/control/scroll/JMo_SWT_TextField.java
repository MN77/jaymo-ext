/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.jaymo_lang.lib.gui.control.scroll.A_JG_TextFieldM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Char;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION_H;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_TextField extends A_SWT_Scrollable<Text> {

	private final A_JG_TextFieldM methods = new A_JG_TextFieldM() {
		@Override
		protected void setAlign(POSITION_H align) {
			JMo_SWT_TextField.this.pendingStyle( align == POSITION_H.LEFT ? SWT.LEFT : align == POSITION_H.RIGHT ? SWT.RIGHT : SWT.CENTER );
		}

		@Override
		protected void setEditable( boolean b ) {
			JMo_SWT_TextField.this.pending( () -> {
				JMo_SWT_TextField.this.swt().setEditable( b );
			} );
		}

		@Override
		protected void setText( String s ) {
			JMo_SWT_TextField.this.pending( () -> {
				JMo_SWT_TextField.this.swt().setText( s );
			} );
		}

		@Override
		protected String getText() {
			return JMo_SWT_TextField.this.swt().getText();
		}
	};


	public JMo_SWT_TextField() {
//		this.s1_Border(true);
		this.pendingStyle( SWT.SINGLE ); // SWT.MULTI
	}

	/**
	 * °@modify # On text modify
	 */
	@Override
	protected void jmoInit4( final CallRuntime cr ) {
		this.registerEvent( "@modify", () -> { // TODO @change?!?
			this.swt().addListener( SWT.Modify, event -> JMo_SWT_TextField.this.eventRun( cr, "@modify", JMo_SWT_TextField.this ) );
		} );

//		this.swt().addVerifyListener(vl);	// Verify-Event
	}

	/**
	 * °getText()Str # Returns the text of this field.
	 * °setAlign(MagicPosition pos)Same # Set the alignment of the text.
	 * °setEchoChar(Char c)Same # Set a char, which will be displayed instead of the real chars.
	 * °setEditable(Bool b)Same # Set this element editable or readonly.
	 * °setText(Str text)Same # Set the text of this Widget.
	 */
	@Override
	protected I_Object jmoMethod4( final CallRuntime cr, final String method ) {
		switch(method) {
			case "setEchoChar":
				final JMo_Char arg = cr.arg( this, JMo_Char.class );
				char c = arg.rawChar( cr );
				JMo_SWT_TextField.this.pending( () -> {
					JMo_SWT_TextField.this.swt().setEchoChar( c );
				} );
				return this;
		}

		return this.methods.call( cr, method, this );
	}

	@Override
	protected Text swtCreate( final Composite basis, final int style ) {
		return new Text( basis, style );
	}

	@Override
	protected void swtInit4( final CallRuntime cr ) {}

}
