/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.jaymo_lang.ext.swt.widget.control.A_SWT_Control;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_ComboBoxI;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_ComboBoxM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_ComboBox extends A_SWT_Control<Combo> { // TODO Rename to Combo ?!?

	/**
	 * °get ^ getValue
	 * °getValue()Str # Returns the current value
	 * °setItems(Str items...)Same
	 * °clear()Same
	 * °set ^ setValeu
	 * °setValue(Str s)Same
	 * °append ^ add
	 * °add(Str items...)Same # Add one or more strings.
	 * °addAll(List items)Same
	 */
	private final A_JG_ComboBoxM methods = new A_JG_ComboBoxM() {

		@Override
		public void add( final String[] items ) {
			JMo_SWT_ComboBox.this.pending( () -> {
			final int len = items.length;

			for( int i = 0; i < len; i++ ) {
				final String s = items[i];
				JMo_SWT_ComboBox.this.swt().add( s );
				if( i == len - 1 )
					JMo_SWT_ComboBox.this.swt().setText( s );
			}
			});
		}

		@Override
		public void clear() {
			JMo_SWT_ComboBox.this.pending( () -> JMo_SWT_ComboBox.this.swt().removeAll() );
		}

		@Override
		public String getValue() {
			return JMo_SWT_ComboBox.this.swt().getText();
		}

		@Override
		public void setItems( final String[] items ) {
			JMo_SWT_ComboBox.this.pending( () -> {
			JMo_SWT_ComboBox.this.swt().removeAll();

			for( final String s : items )
				JMo_SWT_ComboBox.this.swt().add( s );

			JMo_SWT_ComboBox.this.swt().setText( items[items.length - 1] ); // TODO last/first ?!?
			});
		}

		@Override
		public void setValue( final String s ) {
			JMo_SWT_ComboBox.this.pending( () -> JMo_SWT_ComboBox.this.swt().setText( s ) );
		}
	};

	private final A_JG_ComboBoxI inits = new A_JG_ComboBoxI() {

		@Override
		protected void arg0readOnly( final boolean readOnly ) {
			if( readOnly )
				JMo_SWT_ComboBox.this.pendingStyle( SWT.READ_ONLY );
		}

		@Override
		protected void atChange( final CallRuntime cr, final String ev ) {
			JMo_SWT_ComboBox.this.registerEvent( ev, () -> {
				JMo_SWT_ComboBox.this.swt().addListener( SWT.Modify, event -> JMo_SWT_ComboBox.this.eventRun( cr, ev, JMo_SWT_ComboBox.this ) );
			} );
		}
	};


	private final ArgCallBuffer arg0readOnly;


	/**
	 * +SWT_ComboBox()
	 * +SWT_ComboBox(boolean readOnly)
	 */
	public JMo_SWT_ComboBox() {
		this.arg0readOnly = null;
	}

	public JMo_SWT_ComboBox( final Call readOnly ) {
		this.arg0readOnly = new ArgCallBuffer( 0, readOnly );
	}


	@Override
	public void jmoInit3( final CallRuntime cr ) {
		this.inits.init( cr, this, this.arg0readOnly );
	}

	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected Combo swtCreate( final Composite parent, final int style ) {
		return new Combo( parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

}
