/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.ext.swt.graphic.SWT_Image;
import org.jaymo_lang.lib.graphic.JMo_Image;
import org.jaymo_lang.lib.gui.control.A_JG_LabelI;
import org.jaymo_lang.lib.gui.control.A_JG_LabelM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 * @implNote complete
 */
public class JMo_SWT_Label extends A_SWT_Control<Label> {

	private final ArgCallBuffer arg0;
	private final ArgCallBuffer arg1;
	private final ArgCallBuffer arg2;

	private final A_JG_LabelM methods = new A_JG_LabelM() {

		@Override
		protected I_Object getImage() {
			final Image img = JMo_SWT_Label.this.swt().getImage();
			return img == null
				? Nil.NIL
				: new SWT_Image( img ).toJayMoImage();
		}

		@Override
		protected String getText() {
			return JMo_SWT_Label.this.swt().getText(); // TODO String will be maybe empty
		}

		@Override
		protected void setImage( JMo_Image image ) {
			if(image == null)
				JMo_SWT_Label.this.pending( () -> JMo_SWT_Label.this.swt().setImage( null ) ); // TODO testen!
			else
				JMo_SWT_Label.this.pending( () -> JMo_SWT_Label.this.swt().setImage( new SWT_Image( image ).getImage() ) );
		}

		@Override
		protected void setText( String text ) {
			JMo_SWT_Label.this.pending( () -> JMo_SWT_Label.this.swt().setText( text ) );
		}
	};

	private final A_JG_LabelI inits = new A_JG_LabelI() {

		@Override
		protected void arg0str( JMo_Str s ) {
			JMo_SWT_Label.this.pending( () -> {
				JMo_SWT_Label.this.swt().setText( s.rawString() );
			} );
		}

		@Override
		protected void arg0image( JMo_Image s ) {
			JMo_SWT_Label.this.pending( () -> {
				JMo_SWT_Label.this.swt().setImage( SWT_Image.newDirect( s.internalGet() ).getImage() );
			} );
		}

		@Override
		protected void arg1pos( CallRuntime cr, POSITION pos ) {
			if( pos == POSITION.LEFT )
				JMo_SWT_Label.this.pendingStyle( SWT.LEFT );
			else if( pos == POSITION.CENTER )
				JMo_SWT_Label.this.pendingStyle( SWT.CENTER );
			else if( pos == POSITION.RIGHT )
				JMo_SWT_Label.this.pendingStyle( SWT.RIGHT );
			else
				throw new RuntimeError( cr, "Invalid position", "Allowed are left, center right, but got: " + pos );
		}

		@Override
		protected void arg2wrap( boolean wrap ) {
			if( wrap )
				JMo_SWT_Label.this.pendingStyle( SWT.WRAP );
		}
	};


	/**
	 * +SWT_Label()
	 * +SWT_Label(Str|Image content)
	 * +SWT_Label(Str|Image content, MagicPosition align)
	 * +SWT_Label(Str|Image content, MagicPosition align, Bool wrap)
	 */
	public JMo_SWT_Label() {
		this.arg0 = null;
		this.arg1 = null;
		this.arg2 = null;
	}

	public JMo_SWT_Label( final Call arg ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = null;
		this.arg2 = null;
	}

	public JMo_SWT_Label( final Call arg, final Call align ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = new ArgCallBuffer( 1, align );
		this.arg2 = null;
	}

	public JMo_SWT_Label( final Call arg, final Call align, final Call wrap ) {
		this.arg0 = new ArgCallBuffer( 0, arg );
		this.arg1 = new ArgCallBuffer( 1, align );
		this.arg2 = new ArgCallBuffer( 2, wrap );
	}

	@Override
	public void jmoInit3( final CallRuntime cr ) {
		this.inits.init(cr, this, this.arg0, this.arg1, this.arg2);
	}

	/**
	 * °setText(Str text)Same # Set the text on this label.
	 * °getText()Str # Returns the text of this label.
	 * °setImage(Image text)Same # Set the image on this label.
	 * °getImage()Image? # Returns the image of this lable of nil if nothing is set.
	 */
	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected Label swtCreate( final Composite parent, final int style ) {
		return new Label( parent, style ); // TODO SWT.BORDER
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

}
