/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.widget.control.scroll.composite;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Spinner;
import org.jaymo_lang.ext.swt.widget.control.A_SWT_Control;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_SpinnerI;
import org.jaymo_lang.lib.gui.control.scroll.composit.A_JG_SpinnerM;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 */
public class JMo_SWT_Spinner extends A_SWT_Control<Spinner> {

	/**
	 * °get ^ getvalue
	 * °getValue()Int # Returns the current value
	 * °getMin()Int # Returns the minimum limit
	 * °getMax()Int # Returns the maximum limit
	 * °set ^ setvalue
	 * °setValue(Int value)Same # Set value
	 * °setRange(Range r)Same # Set the minimum and maximum
	 * °setRange(Int Range r)Same # Set the minimum and maximum
	 */
	private final A_JG_SpinnerM methods = new A_JG_SpinnerM() {

		@Override
		protected int getMax() {
			return JMo_SWT_Spinner.this.isCreated()
				? JMo_SWT_Spinner.this.swt().getMaximum()
				: JMo_SWT_Spinner.this.bufferMax;
		}

		@Override
		protected int getMin() {
			return JMo_SWT_Spinner.this.isCreated()
				? JMo_SWT_Spinner.this.swt().getMinimum()
				: JMo_SWT_Spinner.this.bufferMin;
		}

		@Override
		protected int getValue() {
			return JMo_SWT_Spinner.this.isCreated()
				? JMo_SWT_Spinner.this.swt().getSelection()
				: JMo_SWT_Spinner.this.bufferVal;
		}

		@Override
		protected void setMinMax( final int min, final int max ) {
			JMo_SWT_Spinner.this.pending( () -> JMo_SWT_Spinner.this.iSetMinMax( min, max ) );
		}

		@Override
		protected void setValue( final int value ) {
			JMo_SWT_Spinner.this.bufferVal = value;

			JMo_SWT_Spinner.this.pending( () -> {
				JMo_SWT_Spinner.this.swt().setSelection( value );
			} );
		}

	};

	/*
	 * °@select # When the value changes.
	 */
	private final A_JG_SpinnerI inits = new A_JG_SpinnerI() {

		@Override
		protected void argsMinMax( final int min, final int max ) {
			JMo_SWT_Spinner.this.iSetMinMax( min, max );
		}

		@Override
		protected void atChange( final CallRuntime cr, final String ev ) {
			JMo_SWT_Spinner.this.registerEvent( ev, () -> {
				JMo_SWT_Spinner.this.swt().addListener( SWT.Selection, event -> JMo_SWT_Spinner.this.eventRun( cr, ev, JMo_SWT_Spinner.this ) );
			} );
		}

	};


	private final ArgCallBuffer arg0;
	private final ArgCallBuffer arg1;
	private int                 bufferMin = Integer.MIN_VALUE;
	private int                 bufferMax = Integer.MAX_VALUE;
	private int                 bufferVal = 1; // TODO


	/**
	 * +SWT_Spinner()
	 * +SWT_Spinner(Int min, Int max)
	 */
	public JMo_SWT_Spinner() {
		this.arg0 = null;
		this.arg1 = null;
	}

	public JMo_SWT_Spinner( final Call min, final Call max ) {
		this.arg0 = new ArgCallBuffer( 0, min );
		this.arg1 = new ArgCallBuffer( 1, max );
	}


	@Override
	public void jmoInit3( final CallRuntime cr ) {
		this.inits.init( cr, this, this.arg0, this.arg1 );
	}

	@Override
	protected I_Object jmoMethod3( final CallRuntime cr, final String method ) {
		return this.methods.call( cr, method, this );
	}

	@Override
	protected Spinner swtCreate( final Composite parent, final int style ) {
		return new Spinner( parent, style );
	}

	@Override
	protected void swtInit3( final CallRuntime cr ) {}

	private void iSetMinMax( final int min, final int max ) {
		JMo_SWT_Spinner.this.bufferMin = min;
		JMo_SWT_Spinner.this.bufferMax = max;

		JMo_SWT_Spinner.this.pending( () -> {

			for( int i = 0; i <= 1; i++ ) { // Set it 2 times
				JMo_SWT_Spinner.this.swt().setMaximum( max );
				JMo_SWT_Spinner.this.swt().setMinimum( min );
			}
		} );
	}

}
