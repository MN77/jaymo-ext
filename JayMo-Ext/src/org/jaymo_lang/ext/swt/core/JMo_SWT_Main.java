/*******************************************************************************
 * Copyright (C) 2010-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.swt.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Widget;
import org.jaymo_lang.ext.swt.dialog.SWT_Error;
import org.jaymo_lang.ext.swt.widget.control.scroll.composite.canvas.deco.SWT_Window;
import org.jaymo_lang.lib.gui.control.scroll.composit.canvas.deco.A_JG_MainM;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 13.12.2020
 * @implNote 16.06.2009 Shutdownhook wird nur gesetzt, wenn benötigt
 */
public class JMo_SWT_Main extends A_SWT_Window {

	private int                               height = 200;
	private I_SWT_Object<? extends Composite> layout = null;
	private String                            title  = null;
	private int                               width  = 200;
//	private I_SWT_Window window;

	private final A_JG_MainM methods = new A_JG_MainM() {

		@Override
		protected void run(CallRuntime cr) {
			try {
				cr.getApp().registerFork(); // This must be before and outside the SWT-Thread

				final Thread thread = new Thread( () -> {
					((SWT_Window)JMo_SWT_Main.this.createMainWindow( JMo_SWT_Main.this )).start( cr, () -> {
//						TODO instanz.eventStart(EVENTS.READY, null);
						JMo_SWT_Main.this.eventRun( cr, "@ready", JMo_SWT_Main.this );
					} );
					cr.getApp().checkExit( true );
				} );
				thread.start();
			}
			catch( final Throwable t ) {
				Err.show( t );
				SWT_Error.exit( cr, t ); //TODO für JM° okay?!?
			}
		}
	};

	public final Composite createSWT( final CallRuntime cr, final Widget parent ) {
		Err.ifNull( parent );
		this.layout = this.layout();
		if( this.layout == null )
			Err.direct( "Layout is null!", this.getClass().getName() );
		this.layout.createSWT( cr, parent );
		this.swt().layout();
//		this.initStyle();
//		this.initEvents();
//		this.initIntern();
		return this.swt();
	}

//	public void sFokus() {
//		swt().setFocus();
//	}

	@Override
	public void init( final CallRuntime cr ) {}

//	protected enum EVENTS {
//		CLOSE_SHELL,
//		READY,
//		SHUTDOWN
//	}

//	public void close() {
//	//	this.eventStart(EVENTS.CLOSE_SHELL);
//		this.getWindow().swt().dispose();
//	}

//	public void eEndeFenster(final Supplier<Boolean> e) {
//		if(this.endefenster_extern != null)
//			Err.forbidden("Wurde bereits gesetzt!");
//		this.endefenster_extern = e;
//	}

//	public void eSchliessen(final Consumer<?> z) {
//	//	this.eventAdd(EVENTS.CLOSE_SHELL, z);
//	}

//	@Override
//	protected final void initIntern() {
//		final Supplier<Boolean> z = this.endefenster_extern;
//
//		this.getWindow().swt().addShellListener(new ShellAdapter() {
//			@Override
//			public void shellClosed(final ShellEvent event) {
//				Boolean erg = null;
//				if(z != null)
//					erg = z.get();
//				if(erg != null && !erg)
//					event.doit = false;
//				A_MN_Fenster.this.eventStart(EVENTS.CLOSE_SHELL);
//			}
//		});
//	}

	@Override
	public Composite swt() {
		// TODO Wirklich null ?!?
		return this.layout == null ? null : this.layout.swt();
	}

	@Override
	public boolean validateEvent( final String event ) {
		return event.equals( "@ready" );
	}

	/**
	 * °add(SWT_Object obj)Same # Add component
	 * °setSize(IntNumber width, IntNumber height)Same # Set window size
	 * °setTitle(Str title)Same # Set window title
	 * °run()Same # Start and open main window
	 * °close()Same # Close main window
	 */
	@Override
	protected I_Object callMethod2( final CallRuntime cr, final String method ) {
		//TODO Für alle Funktionen einen automatischen Puffer, der beim erstellen abgearbeitet wird und danach direkt funktioniert!

		return this.methods.call( cr, method, this );
	}

	private I_SWT_Frame createMainWindow( final JMo_SWT_Main instanz ) {
		final SWT_Window w = new SWT_Window( instanz );

//		.s1_FesteGroesse()
//		f.sBild(ICON._24.OBJ_ERDE);
		try {
			w.setImages( new FileInputStream( this.getClass().getClassLoader().getResource( "jar/logo/jaymo_icon_256.png" ).getFile() ) );
		}
		catch( final FileNotFoundException e ) {
			Err.show( e );
		}
		w.setSize( this.width, this.height );
//		f.sTitel("JM°-SWT-Test");
		if( this.title != null )
			w.setTitle( this.title );

//		this.window = w;
		return w;
	}

}
