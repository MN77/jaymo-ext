/*******************************************************************************
 * Copyright (C) 2022-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext;

import org.jaymo_lang.apix.HELP_FILE;
import org.jaymo_lang.apix.TypeDictionary;
import org.jaymo_lang.ext.db.JMo_DBResult;
import org.jaymo_lang.ext.db.JMo_HSqlDB;
import org.jaymo_lang.ext.db.JMo_MySQL;
import org.jaymo_lang.ext.db.JMo_SQLite3;
import org.jaymo_lang.ext.ftp.JMo_FtpClient;
import org.jaymo_lang.ext.ftp.JMo_FtpRemotePath;
import org.jaymo_lang.ext.ftp.JMo_FtpsClient;
import org.jaymo_lang.ext.jci.JMo_JCI_Janino;
import org.jaymo_lang.ext.jci.JMo_JCI_Result;
import org.jaymo_lang.ext.jci.JMo_JCI_Source;
import org.jaymo_lang.ext.jci.JMo_JCI_Target;
import org.jaymo_lang.ext.marytts.JMo_MaryTTS;
import org.jaymo_lang.ext.neuroph.JMo_Neuroph_MultiLayerPerceptron;
import org.jaymo_lang.ext.neuroph.JMo_Neuroph_Perceptron;
import org.jaymo_lang.ext.neuroph.JMo_Neuroph_TrainSet;
import org.jaymo_lang.ext.pdf.create.JMo_IText_Document;
import org.jaymo_lang.ext.pdf.create.constant.JMo_IText_Font;
import org.jaymo_lang.ext.pdf.create.constant.JMo_IText_PageSize;
import org.jaymo_lang.ext.pdf.create.page.JMo_IText_Page;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemCircle;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemImage;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemLine;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemRectangle;
import org.jaymo_lang.ext.pdf.create.page.item.JMo_IText_ItemText;
import org.jaymo_lang.ext.pdf.create.text.JMo_IText_TextChunk;
import org.jaymo_lang.ext.pdf.create.text.JMo_IText_TextParagraph;
import org.jaymo_lang.ext.pdf.create.text.JMo_IText_TextPhrase;
import org.jaymo_lang.ext.pdf.fill.JMo_PdfFiller;
import org.jaymo_lang.ext.swt.core.JMo_SWT_Main;
import org.jaymo_lang.ext.swt.layout.JMo_SWT_LayoutBorder;
import org.jaymo_lang.ext.swt.layout.JMo_SWT_LayoutGrid;
import org.jaymo_lang.ext.swt.widget.control.JMo_SWT_Button;
import org.jaymo_lang.ext.swt.widget.control.JMo_SWT_Label;
import org.jaymo_lang.ext.swt.widget.control.JMo_SWT_ProgressBar;
import org.jaymo_lang.ext.swt.widget.control.JMo_SWT_Scale;
import org.jaymo_lang.ext.swt.widget.control.JMo_SWT_Slider;
import org.jaymo_lang.ext.swt.widget.control.scroll.JMo_SWT_TextField;
import org.jaymo_lang.ext.swt.widget.control.scroll.composite.JMo_SWT_ComboBox;
import org.jaymo_lang.ext.swt.widget.control.scroll.composite.JMo_SWT_Spinner;
import org.jaymo_lang.object.A_Object;


/**
 * @author Michael Nitsche
 * @created 20.06.2022
 */
public class Types implements TypeDictionary {

	public String[] allTypes() {
		return new String[]{ "DBResult", "FtpClient", "FtpRemotePath", "FtpsClient", "HSqlDB", "IText_Document", "IText_Font", "IText_ItemCircle", "IText_ItemImage", "IText_ItemLine",
			"IText_ItemRectangle", "IText_ItemText", "IText_Page", "IText_PageSize", "IText_TextChunk", "IText_TextParagraph", "IText_TextPhrase", "JCI_Janino", "JCI_Result", "JCI_Source",
			"JCI_Target", "MaryTTS", "MySQL", "Neuroph_MultiLayerPerceptron", "Neuroph_Perceptron", "Neuroph_TrainSet", "PdfFiller", "SQLite3", "SWT_Button", "SWT_Font", "SWT_Label",
			"SWT_LayoutBorder", "SWT_LayoutGrid", "SWT_Main", "SWT_ProgressBar", "SWT_Scale", "SWT_Slider", "SWT_TextField" };
	}

	public Class<? extends A_Object> lookup( final String typename ) {

		switch( typename ) {
			case "DBResult":
				return JMo_DBResult.class;
			case "FtpClient":
				return JMo_FtpClient.class;
			case "FtpRemotePath":
				return JMo_FtpRemotePath.class;
			case "FtpsClient":
				return JMo_FtpsClient.class;
			case "HSqlDB":
				return JMo_HSqlDB.class;
			case "IText_Document":
				return JMo_IText_Document.class;
			case "IText_Font":
				return JMo_IText_Font.class;
			case "IText_ItemCircle":
				return JMo_IText_ItemCircle.class;
			case "IText_ItemImage":
				return JMo_IText_ItemImage.class;
			case "IText_ItemLine":
				return JMo_IText_ItemLine.class;
			case "IText_ItemRectangle":
				return JMo_IText_ItemRectangle.class;
			case "IText_ItemText":
				return JMo_IText_ItemText.class;
			case "IText_Page":
				return JMo_IText_Page.class;
			case "IText_PageSize":
				return JMo_IText_PageSize.class;
			case "IText_TextChunk":
				return JMo_IText_TextChunk.class;
			case "IText_TextParagraph":
				return JMo_IText_TextParagraph.class;
			case "IText_TextPhrase":
				return JMo_IText_TextPhrase.class;
			case "JCI_Janino":
				return JMo_JCI_Janino.class;
			case "JCI_Result":
				return JMo_JCI_Result.class;
			case "JCI_Source":
				return JMo_JCI_Source.class;
			case "JCI_Target":
				return JMo_JCI_Target.class;
			case "MaryTTS":
				return JMo_MaryTTS.class;
			case "MySQL":
				return JMo_MySQL.class;
			case "Neuroph_MultiLayerPerceptron":
				return JMo_Neuroph_MultiLayerPerceptron.class;
			case "Neuroph_Perceptron":
				return JMo_Neuroph_Perceptron.class;
			case "Neuroph_TrainSet":
				return JMo_Neuroph_TrainSet.class;
			case "PdfFiller":
				return JMo_PdfFiller.class;
			case "SQLite3":
				return JMo_SQLite3.class;
			case "SWT_Button":
				return JMo_SWT_Button.class;
			case "SWT_ComboBox":
				return JMo_SWT_ComboBox.class;
			case "SWT_Label":
				return JMo_SWT_Label.class;
			case "SWT_LayoutBorder":
				return JMo_SWT_LayoutBorder.class;
			case "SWT_LayoutGrid":
				return JMo_SWT_LayoutGrid.class;
			case "SWT_Main":
				return JMo_SWT_Main.class;
			case "SWT_ProgressBar":
				return JMo_SWT_ProgressBar.class;
			case "SWT_Scale":
				return JMo_SWT_Scale.class;
			case "SWT_Slider":
				return JMo_SWT_Slider.class;
			case "SWT_Spinner":
				return JMo_SWT_Spinner.class;
			case "SWT_TextField":
				return JMo_SWT_TextField.class;
		}

		return null;
	}

	public String helpFile( HELP_FILE hf ) {
		return switch(hf) {
			case NEW -> "/org/jaymo_lang/ext/constructors.txt";
			case EXTENDS -> "/org/jaymo_lang/ext/extends.txt";
			case FUNCTIONS -> "/org/jaymo_lang/ext/functions.txt";
			case TYPES -> "/org/jaymo_lang/ext/types.txt";
		};
	}

}
