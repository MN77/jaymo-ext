/*******************************************************************************
 * Copyright (C) 2024-2025 Michael Nitsche <code@mn77.de>
 *
 * This file is part of the JayMo-External-Library <https://www.jaymo-lang.org>
 *
 * JayMo-External-Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JayMo-External-Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo-External-Library. If not, see <https://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.ext.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.A_EventObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.JMo_Int;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;
import de.mn77.base.thread.A_Thread;


/**
 * @author Michael Nitsche
 * @created 08.12.2023
 */
public class JMo_JettyHttpServer extends A_EventObject {

	private Server              server = null;
	private final ArgCallBuffer portCall;


	/**
	 * +JettyHttpServer( Int port )
	 * !Simple Http-Server using Jetty
	 */
	public JMo_JettyHttpServer( final Call arg ) {
		this.portCall = new ArgCallBuffer( 0, arg );
	}


	@Override
	public void init( final CallRuntime cr ) {
		this.portCall.init( cr, this, A_IntNumber.class );
	}

	@Override
	public boolean validateEvent( final String event ) {
		return event.equals( "@request" );
	}

	@Override
	protected I_Object callMethod( final CallRuntime cr, final String method ) {

		switch( method ) {
			case "start":
				return this.mStart( cr );
			case "stop":
				return this.mStop( cr );
//			case "join":
//				// Problem: On stop or destroy, the server will stop but the thread keeps running and don't exit
//				return this.mJoin( cr ) );
		}
		return null;
	}

	/**
	 * #°join()Same | Wait until the server will be stopped
	 */
//	private I_Object mJoin( final CallRuntime cr ) {
//		cr.argsNone();
//
//		if( this.server == null )
//			throw new ExternalError( cr, "Server failure", "Join impossible, server is not running" );
//
//		try {
//			this.server.join();
//		}
//		catch( final Exception e ) {
//			throw new ExternalError( cr, "Server failure", e.getMessage() );
//		}
//
//		return this;
//	}

	private void iStop( final CallRuntime cr ) { //, boolean destroy
		final Thread t = new A_Thread() {

			@Override
			public void task() {

				try {
					JMo_JettyHttpServer.this.server.stop();
//					if(destroy)
//						JMo_JettyHttpServer.this.server.destroy(); // server.stop is needed first! It seems, this makes no difference.
				}
				catch( final InterruptedException ie ) {
					Err.show( ie );
				}
				catch( final Throwable e ) {
//					Lib_Error.handleThreadErrorEnd( cr, t );
//					if(destroy)
//						cr.warning( "Server destroyed!", e.getMessage() );
//					else
					throw new ExternalError( cr, "Server stop error", e.getMessage() );
				}
				finally {
					JMo_JettyHttpServer.this.server = null;
					cr.getApp().checkExit( true ); // Jetty-Fork
				}
			}

		};
		t.start();
	}

	/**
	 * °start()Same | Start this http server
	 */
	private I_Object mStart( final CallRuntime cr ) {
		cr.argsNone();
		final int port = Lib_Convert.toInt( cr, this.portCall.get() );

		if( this.server != null )
			cr.warning( "Server start failure", "This server is already running" );
		else
			try {
				// Initialize Jetty-Server
				this.server = new Server();

				final ServerConnector connector = new ServerConnector( this.server,
					new org.eclipse.jetty.server.HttpConnectionFactory() );
				connector.setPort( port );

				this.server.addConnector( connector );
				this.server.setHandler( new JettyHandler( cr, this ) );
				this.server.setStopAtShutdown( true );

				// Start Server
				this.server.start();
				cr.getApp().registerFork(); // Jetty-Fork

				cr.getApp().addShutdownHook( () -> {
					if( this.server != null )
						this.iStop( cr ); //, true
				} );
			}
			catch( final Exception e ) {
				throw new ExternalError( cr, "Server start failure", e.getMessage() );
			}
		return this;
	}

	/**
	 * °stop()Same | Stop this http server
	 * °stop(Int maxDelayMSec)Same | Stop this http server in 'maxDelayMSec' milliseconds.
	 */
	private I_Object mStop( final CallRuntime cr ) {
		final I_Object[] args = cr.argsFlex( this, 0, 1 );

		if( args.length == 1 ) {
			final int value = cr.argType( args[0], JMo_Int.class ).rawInt( cr );
			this.server.setStopTimeout( value ); // Default = 30000l
		}

		if( this.server == null )
			cr.warning( "Server stop failure", "Server is not running" );
		else
			this.iStop( cr ); //, false

		return this;
	}

}
